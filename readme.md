# Ansible Deployment Test - Rocky

- Installer [Ubuntu server 18.04 (Bionic Beaver) LTS 64-bit](http://releases.ubuntu.com/18.04/) sur chaque machine

## Sommaire

 - [Configuration réseau préliminaire](#configuration-réseau-préliminaire)
 - [Partage de clés SSH ](#partage-de-clés-ssh)
 - [Préparation à la mise en place d'OpenStack](#préparation-à-la-mise-en-place-dopenstack)
 - [Configuration réseau des bridges](#configuration-réseau-des-bridges)
 - [Installation OpenStack](#installation-openstack)
 - [Partie DEBUG](#partie-debug)
 - [Test de fonctionnement d'OpenStack](#test-de-fonctionnement-dopenstack)
 - [Test de migration](#test-de-migration)
 - [Annexe](#annexe)
    - [Networking Configuration](#networking-configuration)
        - [Network CIDR et VLAN assignments](#network-cidr-et-vlan-assignments)
        - [IP assignments](#ip-assignments)
        - [Switch Informations](#switch-informations)
    - [Bridges Informations](#bridges-informations)


> **INFORMATION : Les commandes présentes dans ce fichier entre accolades { } sont facultatives.**

> **INFORMATION : Les caractères compris entre 2 tildes (~) sont seulement des indications et ne font pas partis des commandes.**


## Configuration réseau préliminaire

<img src="./Images/schema-reseau-git.png" width="700" height="600">

Assurez vous que votre configuration réseau est fonctionnelle et que les machines communiquent bien entre elles. Configuration en `Annexe` : [Networking Configuration](#networking-configuration)

***
Vous trouvez la configuration du fichier `/etc/netplan/01-netcfg.yaml` de chaque machine dans le dossier [Network](./Network) (qui sera modifié ultérieusement par des bridges)  <br> 
- Une fois le fichier neplan modifié, ne pas oublier d’appliquer les modifications dans les fichiers :

        # netplan apply
        # systemctl restart systemd-networkd 

- N'oubliez pas également de configurer le NAT et d'ajouter le routage sur votre machine `Infra` :

        # iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
        # echo 1 > /proc/sys/net/ipv4/ip_forward  
        
- (Pour le mettre en dur, modifier le fichier '/etc/sysctl.conf' et décommenter la ligne `net.ipv4.ip_forward = 1`, effectuer la commande `sysctl -p` puis redémarrer le service réseau) <br> <br>

Pensez également à ajouter le serveur DNS 8.8.8.8 dans le fichier `/etc/resolv.conf` afin de pouvoir effectuer la redirection de nom pour la suite de l'install.

<p>&nbsp; </p>

## Partage de clés SSH 

Partager les clés ssh (root) entre chaque machine pour ne plus avoir à taper la passphrase à chaque connexion SSH
***
- Effectuer la commande suivante sur votre `Machine Infra` et votre `Machine Physique` :

        # ssh-keygen  

Puis partager la sur vos autres machines :
- Depuis la Machine Physique vers Infra : 

        # ssh-copy-id root@10.10.3.92

- Depuis Infra vers Ceph et Compute : 

        # ssh-copy-id root@10.10.11.21
        # ssh-copy-id root@10.10.11.101

<p>&nbsp; </p>

## Préparation à la mise en place d'OpenStack

Vous pouvez consulter les [différentes versions d'OpenStack](https://releases.openstack.org/) ainsi que les [repos mis à disposition sur Github](https://releases.openstack.org/teams/openstackansible.html).
***

- Sur chacune des machines :

        # apt-get update
        # apt-get dist-upgrade
        # reboot 
<br>

- Sur la Machine Infra :

        # apt-get install aptitude build-essential git ntp ntpdate openssh-server python-dev sudo
        # git clone -b 18.1.6 https://git.openstack.org/openstack/openstack-ansible /opt/openstack-ansible ~~ Choisir le bon repo : version 18.1.6 pour ma part ~~
        # cd /opt/openstack-ansible
        # scripts/bootstrap-ansible.sh
        # cp -r /opt/openstack-ansible/etc/openstack_deploy/* /etc/openstack_deploy/  ~~ Créer le dossier si non existant ~~
<br> 

- Sur les hosts (compute et ceph) :

        # apt-get install bridge-utils debootstrap ifenslave ifenslave-2.6 lsof lvm2 ntp ntpdate openssh-server sudo tcpdump vlan python
        # echo 'bonding' >> /etc/modules
        # echo '8021q' >> /etc/modules
        # service ntp restart
        # reboot

<p>&nbsp; </p>

## Configuration réseau des bridges

<img src="./Images/schema-reseau-bridges-git.png" width="700" height="600">

<br>

On commence par configurer tous les bridges avant le bridge “br-mgmt” pour ne pas endommager le réseau.

Effectivement, si le bridge du Management devait ne pas être configuré correctement sur les différentes machines, nous n’aurions plus accès aux hosts par la suite (puisque l’on se connecte aux hosts depuis la machine infra1). 
Ne pas hésiter à regarder l'annexe sur les [informations des différents bridges](#bridges-informations) pour vous aider à les configurer. <br> 
 
***

Vous trouverez la nouvelle configuration du fichier `/etc/netplan/01-netcfg.yaml` de chacune des machines dans le dossier [Network Bridges](./Network_Bridges)

Si vous rencontrez des difficultés avec la mise en place des bridges (connectivité), voir la partie [débug](#partie-debug). Vérifier que les interfaces sont bien attribuées à chaque bridge avec la commande :

        # brctl show br-storage

Pour la suite, nous avons besoin d'interfaces virtuelles, netplan ne gèrant plus les interfaces virtuelles, nous créons une interface avec 2 adresses IP (pour br-mgmt et eth1 sur `Infra`). 

***

Pour que la connexion soit plus rapide entre les machines hosts (ceph et compute) et votre machine Infra, vous pouvez installer un proxy sur Infra. 
Voir la [documentation](https://doc.ubuntu-fr.org/apt-cacher). (Pour l'installation d'Apache, il faudra modifier le port par défaut qui est 80 et le remplacer par 8080 par exemple car le service haproxy configuré par openstack utilise ce même port)

<p>&nbsp; </p>

## Installation OpenStack

> Les opérations suivantes sont à effectuer sur la `Machine Infra`

***

Configurer les fichiers nécessaires à la mise en place d'OpenStack. 
Nous avons deux fichiers principaux à configurer où l’on doit renseigner les différents réseaux (Management, Guest et Storage), renseigner les principales adresses IP ainsi que les 4 bridges.
Vous trouverez ces fichiers dans le dossier `/etc/openstack_deploy` de votre machine. Des fichiers .yml.example existent afin de vous donner un exemple de configuration possible.  
<p> </p>

- Copiez ce fichier exemple de la configuration OpenStack :

        # cd /etc/openstack_deploy`

- Copier les playbooks nécessaires à la mise en place d'openstack via le même nom de fichier .yml.example pour qu'ils soient pris en compte dans la configuration :

        # cp openstack_user_config.yml.example openstack_user_config.yml
        ~~ Configuration de CEPH à l'intérieur du fichier user_variables.yml (fin de fichier)
        # cp /env.d/cinder-volume.yml.container.example /env.d/cinder-volume.yml

<br> 

***
<br>
Ensuite, configurer OpenStack selon votre réseau et selon vos besoins. Tous les fichiers ayant besoin d'être modifiés se trouvent dans le dossier [Openstack](./Openstack). 
Les autres n'ont pas besoin de modifications. Si les fichiers ou dossiers n'existent pas dans votre répertoire, créez-les. **Attention**, la version de Ceph-Ansible 
(couramment utilisé : MIMIC) doit correspondre avec la version de Openstack-Ansible.

<p><br> </p>

***

> Utiliser [Screen](https://doc.ubuntu-fr.org/screen) pour lancer les playbooks (le 1er playbooks modifiant les paramètres SSH, il vaut mieux utiliser SCREEN avant tout problème de perte de connexion SSH :)) <br> <br>

- Une fois qu’OpenStack est normalement bien configuré, il suffit d’exécuter ces différentes commandes :

        # cd /opt/openstack-ansible
        # ./scripts/pw-token-gen.py --file /etc/openstack_deploy/user_secrets.yml
        # cd /opt/openstack-ansible/playbooks
        # openstack-ansible setup-infrastructure.yml --syntax-chec
        ~~ Autre commande pour vérifier la syntaxe : ~~
        # /opt/openstack-ansible/inventory/dynamic_inventory.py --check
        # {time} openstack-ansible {-vvv} setup-hosts.yml
<br>

***
<br>

- Avant de lancer l'installation complète d'OpenStack, vous pouvez vérifier la bonne configuration de votre Ceph uniquement via la commande : 

        # {time} openstack-ansible {-vvv} ceph-install.yml`

<p>&nbsp; </p>

- Continuer l'installation d'OpenStack :

        # {time} openstack-ansible {-vvv} setup-infrastructure.yml
        # ansible galera_container -m shell -a "mysql -h localhost -e 'show status like \"%wsrep_cluster_%\";'"

<p>&nbsp; </p>

- Avant de lancer la dernière étape d'installation, suite à des problèmes entre le LDAP et heat, commentez les lignes suivantes dans le fichier [/etc/ansible/roles/os_heat/tasks/heat_service_setup.yml](./Playbooks/heat_service_setup.yml) puis modifier la ligne suivante dans le fichier `/etc/ansible/roles/os_heat/defaults/main.yml` :

        heat_service_in_ldap = true` 
        ~~ de base, la variable est à false ~~

<p>&nbsp; </p>

- Puis lancer l'étape suivante : 

        # {time} openstack-ansible {-vvv} setup-openstack.yml

<p>&nbsp; </p>

> **Confirmer l'installation d’OpenStack avec zéro élément injoignable ou échoué.**

<br>

<i>**Source : https://docs.openstack.org/openstack-ansible/rocky/**</i>

<br>

# Partie DEBUG

Plusieurs difficultés rencontrées lors de l’installation d’OpenStack :  <br> 
- Erreurs de syntaxe dans les fichiers de configuration (par exemple 1 espace en trop ou en moins dans le fichier)
- Erreurs de compilation
- Problème d’installation du paquet LVM2
- Déconnexion du SSH pendant l’exécution des scripts qui cause des erreurs par la suite

<br> 

***

**Installation LVM**
<br>

> **ATTENTION !!!** Si LVM2 ne s’est pas correctement installé sur n’importe quelle machine, ou même sur toutes les machines (ce qui est très probable sur les versions récentes d’Ubuntu),

- Il faut absolument le réinstaller de manière propre en effectuant les commandes suivantes :

        # apt-get purge lvm2
        # apt autoremove
        # apt-get install lvm2

Après avoir effectué ces manipulations, le paquet devrait s’installer correctement.  <br>

***

**Bridges**
<br>

Personnellement, j'ai eu un problème sur le bridge br-storage, qui ne s'attribuait aucune interface. Exemple : 

        # brctl show br-storage

|      bridge name       |      bridge id     |  STP enabled  |  interfaces  |
|:----------------------:|:------------------:|:-------------:|:------------:|
|       br-storage       | 8000.b64192289a59  |      no       |              |

Effectuez la commande suivante pour affecter l'interface souhaitée (ici vlan12) :

        # brctl addif br-storage vlan12

Le résultat doit être le suivant :

|      bridge name       |      bridge id     |  STP enabled  |  interfaces  |
|:----------------------:|:------------------:|:-------------:|:------------:|
|       br-storage       | 8000.b64192289a59  |      no       |    vlan12    |

<br> 
 
***

**Installation OPENSTACK**
<br>

Si l’installation ne s’est pas correctement déroulée sur un seul des noeuds, il suffit de lancer la commande suivante pour qu’elle s’applique uniquement à ce noeud (ce qui évitera de relancer l’installation complète) :

        # openstack-ansible -vvv name_setup.yml --limit hostname
<br> 

Si une erreur s’est glissée dans votre installation, il est parfois utile de détruire tous les conteneurs et de reconstruire l'AIO. 
Bien qu'il soit préférable que l'AIO soit entièrement détruite et reconstruite, ce n'est pas toujours pratique. En tant que tel, ce qui suit peut être exécuté à la place :

        ~~ Move to the playbooks directory ~~
        # cd /opt/openstack-ansible/playbooks`
        ~~ Destroy all of the running containers ~~
        # openstack-ansible lxc-containers-destroy.yml {--limite hostname}  
<br> 

Autre problème rencontré : Effectuer des changements dans le fichier de configuration d'OpenStack. 
Si on décide de modifier ou encore de ne plus utiliser un container, lors du lancement de la commande de la vérification de la syntaxe, il nous dira qu’il ne trouve plus le container en question. 
On peut donc checker la liste des containers et supprimer celui dont on n’a plus besoin :

        # /root/openstack-ansible-overlay/openstack-ansible/scripts/inventory-manage.py -l 
        ~~ Le fichier "inventory-manage.py" peut se trouver ailleurs, effectuez un locate pour le trouver ~~
        # /root/openstack-ansible-overlay/openstack-ansible/scripts/inventory-manage.py -r nom_container
<br> 

OpenStack a également tendance à modifier pas mal de configuration, n'hésitez pas à vérifier que votre NAT est toujours fonctionnel par exemple. <br> <br>

***

**Erreur flat eth12**

En installant OpenStack, il se peut que les paramètres de l'interface flat eth12 se glissent sur le compute, ce qui amènera forcément à une erreur puisque l'interface eth12 n'existe pas sur le compute.
- Pour résoudre ce problème, il faudra modifier le fichier `/etc/neutron/plugins/ml2/linuxbridge_agent.ini` et supprimer la partie flat :

        # Linux bridge agent physical interface mappings
        
        [linux_bridge]
        physical_interface_mappings = vlan:br-vlan


<p>&nbsp; </p>

# Test de fonctionnement d'OpenStack

Pour tester le bon fonctionnement d'OpenStack, on peut s'y connecter à l'aide de l'utilisateur "admin". Vous trouverez le mot de passe dans le fichier `/etc/openstack_deploy/user_secrets.yml` à la ligne `keystone_auth_admin_password`.
L'adresse pour y accéder par interface graphique est la **10.10.3.93**. <br> 

![connection](./Images/connection.jpg)  <br> 

- On peut ensuite créer un projet en ligne de commande pour qu'on puisse se connecter avec le LDAP. Sans attribution de projet, on ne peut se connecter avec ses identifiants LDAP. Tout d'abord se connecter au container `utility` :

        # lxc-attach infra1_utility_container-dc6f8e64
        # source openrc ~~ activer le système d’initialisation ~~
        # openstack project create --description 'Test Project OpenStack' OpenStack-Test --domain viveris` ~~ créer un projet 'OpenStack-Test' dans le domaine viveris ~~
        # openstack role add --user admin --project OpenStack-Test admin` ~~ attribuer les droits d'admin au projet 'OpenStack-Test' à l'utilisateur admin ~~
<br>

Ensuite, depuis l'interface graphique, on pourra voir apparaître notre nouveau projet :  <br> 

![project](./Images/project.png)  <br> 

Pour pouvoir accéder au projet depuis le domaine viveris, on sélectionne l'action "Manage members" qui permettra d'ajouter un administrateur au projet (par defaut admin-openstack). 
On peut également ajouter d'autres personnes comme "membre du projet". **Ne jamais s'ajouter comme administrateur à un projet mais toujours utiliser un compte admin**  <br> 

|  <img src="./Images/members.jpg" height="620" width="550" >           |
|:---------------------------------------------------------------------:|  

<br>
On peut ensuite se connecter avec le compte admin d'OpenStack sur le domaine viveris. Il faut donc changer le domaine "default" par "viveris" lors de la connexion et entrer les bons identifiants.  <br>

- Vous pouvez et devez ajouter :
    - Une image disque -> "Project" -> "Compute" -> "Image"
    - Un réseau TEST (network) -> "Project" ou "Admin" -> "Network" -> "Networks" 
    - Un gabarit (flavor) -> "Project" -> Compute" -> "Flavor"
    - Un routeur GW -> "Project" ou "Admin" -> "Network" -> "Routers" <br> 

Tout ceci vous servira pour créer vos Instances. Une fois les 4 outils créés, vous pouvez maintenant créer une Instance dans "Projet" -> "Compute" -> "Instances" en choisissant l'image disque que vous avez insérée, le réseau, le routeur ainsi que le gabarit. <br> <br>

Si l'instance démarre correctement et qu'il n'y a pas d'erreurs dans les logs, OpenStack fonctionne correctement. <br> 

- Pour tester correctement le réseau, vous pouvez créer un réseau externe et tester le DHCP sur votre Instance :

        # lxc-attach infra1_utility_container-dc6f8e64`
        # openstack network create --external --provider-network-type vlan --provider-segment 934 --provider-physical-network vlan "VLAN Rx34"
        # neutron subnet-create "VLAN Rx34" 10.10.34.0/24 --allocation-pool start=10.10.34.128,end=10.10.34.143 --disable-dhcp --gateway 10.10.34.1
<br>

Il suffit ensuite d'attacher le routeur aux 2 interfaces (réseau externe 10.10.34.0/24 + réseau test 10.10.40.0/24). <br> <br>

# Test de migration

La migration est assez simple, il suffit de se mettre dans la branche désirée. Nous sommes en version Rocky et désirons passer à la dernière version qui est la Stein. 
- Nous pouvons donc exécuter les commandes suivantes :

        # cd /opt/openstack-ansible
        # git branch 19.0.0.0rc2 
<br> 

Avant de lancer le script d'upgrade, vous pouvez ajouter une ligne dans ce script avant de passer une étape de vérification des fichiers. 
- Il va sûrement fail en raison des différents ajouts de fichiers dans le dossier `/env.d` d'OpenStack.

        # vim ./scripts/run-upgrade.sh
        
        RUN_TASKS+=("${SCRIPTS_PATH}/upgrade-utilities/deploy-config-changes.yml --skip-tags custom-envd-file-check") 
        ~~ Ajouter l'option du skip-tags ligne 171 ~~
<br> 

- Il ne nous reste plus qu'à lancer le script :

        # ./scripts/run-upgrade.sh
<br>

<i>**Source : https://docs.openstack.org/openstack-ansible/latest/admin/upgrades/major-upgrades.html**</i>

# Annexe
 
## Networking Configuration

### Network CIDR et VLAN assignments

|         Network        |       CIDR      |  VLAN   |
|:----------------------:|:---------------:|:-------:|
|   Management Network   | 10.10.11.0/24   |    11   |
|     Storage Network    | 10.10.12.0/24   |    12   |
|      Guest Network     | 10.10.13.0/24   |  12,13  |

### IP assignments

| Host name | Management IP |   Storage IP  |    Guest IP   |
|:---------:|:-------------:|:-------------:|:-------------:|
|   infra1  |  10.10.11.1   |               |  10.10.13.1   |
|  compute1 |  10.10.11.101 |  10.10.12.101 |  10.10.13.101 |
|    ceph1  |  10.10.11.21  |  10.10.12.21  |               |

### Switch Informations

<img src="./Images/Switch-infos.png" height="100" width="700" >

*Les VLANS correspondent aux bridges selon la couleur sur le schéma. Port 44 non utilisé.


## Bridges Informations

|     Nom de pont    |                       Meilleure configuration                  |    Avec une IP statique      |
|:------------------:|:--------------------------------------------------------------:|:----------------------------:|
|       br-mgmt      |  Sur chaque noeud                                              |    Toujours                  |
|       br-storage   |  Sur chaque noeud storage  <p></p>  Sur chaque noeud compute   |    Toujours <p></p> Toujours |
|       br-vxlan     |  Sur chaque noeud infra    <p></p>  Sur chaque noeud compute   |    Toujours <p></p> Toujours |
|       br-vlan      |  Sur chaque noeud infra    <p></p>  Sur chaque noeud compute   |    Jamais  <p></p> Jamais    |


<br> <br>


|  ⬆ [Remonter en haut de la page](#sommaire)  |
|---------------------------------------------:|
